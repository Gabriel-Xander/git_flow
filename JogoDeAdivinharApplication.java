package com.jogoDeAdivinhar.jogoDeAdivinhar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;
import java.util.Scanner;

@SpringBootApplication
public class JogoDeAdivinharApplication {

    public static void main(String[] args) {
        SpringApplication.run(JogoDeAdivinharApplication.class, args);
        jogarJogoDeAdivinhar();
    }

    public static void jogarJogoDeAdivinhar() {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        int numeroAleatorio = random.nextInt(100) + 1;
        int tentativas = 0;
        int palpite;

        System.out.println("Bem-vindo ao Jogo de Adivinhação! Tente adivinhar o número entre 1 e 100.");

        do {
            System.out.print("Digite seu palpite: ");
            palpite = scanner.nextInt();
            tentativas++;

            if (palpite > numeroAleatorio) {
                System.out.println("Palpite muito alto. Tente novamente.");
            } else if (palpite < numeroAleatorio) {
                System.out.println("Palpite muito baixo. Tente novamente.");
            } else {
                System.out.println("Parabéns! Você acertou o número em " + tentativas + " tentativas.");
            }
        } while (palpite != numeroAleatorio);

        scanner.close();
    }
}
